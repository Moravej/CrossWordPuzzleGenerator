﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrossWordPuzzleGenerator.Models;

namespace CrossWordPuzzleGenerator.Controllers
{
    public class WordPuzzleController : Controller
    {
        DbDataContext db = new DbDataContext();
        // GET: WordPuzzle
        public ActionResult Index()
        {
            var puzzles = db.WordPuzzles;
            return View(puzzles);
        }

        // GET: WordPuzzle/Details/5
        public ActionResult Details(int id)
        {
            var puzzle = db.WordPuzzles.Where(c => c.id == id).SingleOrDefault();
            try
            {
                string[] characters = puzzle.PuzzleContent.Split(',');
                char[,] ch = new char[puzzle.Dimention, puzzle.Dimention];
                for (int i = 0; i < puzzle.Dimention; i++)
                {
                    for (int j = 0; j < puzzle.Dimention; j++)
                    {
                        ch[i, j] = characters[i * puzzle.Dimention + j][0];
                    }
                }
                ViewBag.Puzzle = ch;

            }
            catch
            {

            }
            return View(puzzle);
        }
        
        public ActionResult Documents()
        {
            return View();
        }
    }
}
