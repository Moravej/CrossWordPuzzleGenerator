﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using CrossWordPuzzleGenerator.Models;
using static CrossWordPuzzleGenerator.Models.ModelClasses;

namespace CrossWordPuzzleMaker.Controllers
{
    public class GenerateAPuzzleController : ApiController
    {
        // POST: api/GenerateAPuzzle
        public JObject Post([FromBody]FormDataCollection formData)
        {
            dynamic ReturnValue = new JObject();
            var Allwords = formData.Get("Words");
            var personid_InString = formData.Get("personid");
            var name = formData.Get("name");

            try
            {

                int personid = 0;
                if (!Int32.TryParse(personid_InString, out personid))
                    throw new Exception("personid is not integer");

                Allwords = Allwords.Replace(",", "،");
                Allwords = Allwords.Replace("،،", "،");
                Allwords = Allwords.Replace(" ", "");
                Allwords = Allwords.Replace(".", "");
                Allwords = Allwords.Replace("-", "");
                int dimention = 0;
                char[,] ch;
                string[] words = Allwords.Split('،');
                Random rnd = new Random();
                words = words.OrderByDescending(c => c.Length).ToArray();
                dimention = words.First().Length;
                bool result = false;
                WordInView[,] wiv = new WordInView[dimention, dimention];
                while (true)
                {
                    ch = new char[dimention, dimention];
                    Utilities.EmptyPuzzle(ch);
                    result = Utilities.GeneratePuzzle(words, ch);
                    if (result)
                    {
                        char[,] answers = new char[dimention, dimention];
                        for (int i = 0; i < ch.GetLength(0); i++)
                        {
                            for (int j = 0; j < ch.GetLength(1); j++)
                            {
                                if (ch[i, j] == '-' || ch[i, j] == '\0')
                                {
                                    answers[i, j] = '\0';
                                }
                                else
                                {
                                    answers[i, j] = ch[i, j];
                                }
                            }
                        }
                        Utilities.FillWithRandomLetters(ch);
                        break;
                    }
                    else
                    {
                        //words = words.OrderBy(x => rnd.Next()).ToArray();
                        Utilities.EmptyPuzzle(ch);
                        dimention++;
                    }
                }

                #region Convert to Json
                string puzzle = null;
                JArray Table = new JArray();
                for (int i = 0; i < ch.GetLength(0); i++)
                {
                    string row = null;
                    JArray FirstDimention = new JArray();
                    for (int j = 0; j < ch.GetLength(1); j++)
                    {
                        row = row + ch[i, j].ToString() + ",";
                    }
                    puzzle = puzzle + row;
                    row.TrimEnd(',');
                    row.TrimStart(',');
                    Table.Add(row);
                }
                #endregion
                DbDataContext db = new DbDataContext();
                WordPuzzle wp = new WordPuzzle();
                wp.Dimention = dimention;
                wp.PuzzleContent = puzzle;
                wp.PersonId = personid;
                wp.name = name;
                wp.Words = Allwords;
                db.WordPuzzles.InsertOnSubmit(wp);
                db.SubmitChanges();
                ReturnValue.result = true;
                ReturnValue.id = wp.id;
                ReturnValue.Dimention = dimention;
                ReturnValue.Table = Table;
            }
            catch (Exception ex)
            {
                ReturnValue.result = false;
                ReturnValue.message = ex;
            }
            return ReturnValue;
        }
    }
}
