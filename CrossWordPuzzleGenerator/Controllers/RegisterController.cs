﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using CrossWordPuzzleGenerator.Models;
using System.Web.Http;

namespace CrossWordPuzzleGenerator.Controllers
{
    public class RegisterController : ApiController
    {
        // POST: api/Register
        public JObject Post([FromBody]FormDataCollection formData)
        {
            dynamic returnValue = new JObject();
            var Name = formData.Get("name");
            var Pass = formData.Get("password");
            try
            {
                DbDataContext db = new DbDataContext();
                var names = (from c in db.Persons
                             select c.UserName).ToList();
                if (names.Contains(Name))
                    throw new Exception("این نام قبلا استفاده شده است");
                Person p = new Person();
                p.UserName = Name;
                p.password = Pass;
                db.Persons.InsertOnSubmit(p);
                db.SubmitChanges();
                returnValue.result = true;
                returnValue.personid = p.id;
            }
            catch (Exception ex)
            {
                returnValue.result = false;
                returnValue.Message = ex.Message;
            }
            return returnValue;
        }
    }
}
