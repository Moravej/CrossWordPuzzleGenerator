﻿using CrossWordPuzzleGenerator.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace CrossWordPuzzleGenerator.Controllers
{
    public class GetPuzzleContentController : ApiController
    {
        // POST: api/GetPuzzleContent
        public JObject Post([FromBody]FormDataCollection formData)
        {
            dynamic ReturnValue = new JObject();
            var puzzleid_inString = formData.Get("PuzzleId");
            DbDataContext db = new DbDataContext();

            int puzzleid = 0;
            if (!Int32.TryParse(puzzleid_inString, out puzzleid))
                throw new Exception("puzzle id is not integer or empty");

            var puzzle = db.WordPuzzles.Where(c => c.id == puzzleid).Single();

            string[] characters = puzzle.PuzzleContent.Split(',');
            char[,] ch = new char[puzzle.Dimention, puzzle.Dimention];
            JArray content = new JArray();
            for (int i = 0; i < puzzle.Dimention; i++)
            {
                string row = null;
                for (int j = 0; j < puzzle.Dimention; j++)
                {
                    row = row + characters[i * puzzle.Dimention + j] + ',';
                }
                row.TrimEnd(',');
                row.TrimStart(',');
                content.Add(row);
            }
            ReturnValue.content = content;
            return ReturnValue;
        }
    }
}
