﻿using CrossWordPuzzleGenerator.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace CrossWordPuzzleGenerator.Controllers
{
    public class SearchPuzzlesWithWordsController : ApiController
    {
        // POST: api/SearchPuzzlesWithWords
        public JObject Post([FromBody]FormDataCollection formData)
        {
            dynamic ReturnValue = new JObject();

            var word = formData.Get("Word");
            string[] words = word.Split('،');
            DbDataContext db = new DbDataContext();
            try
            {
                List<ModelClasses.SortSearchClass> SSC_List = new List<ModelClasses.SortSearchClass>();
                var puzzle = db.WordPuzzles.ToList();
                foreach(var p in puzzle)
                {
                    ModelClasses.SortSearchClass SSC = new ModelClasses.SortSearchClass();
                    foreach (var w in words)
                    {
                        if (p.Words.Contains(w))
                        {
                            SSC.NoOfContainingWords++;
                        }
                    }
                    SSC.wordpuzzle = p;
                    SSC_List.Add(SSC);
                }
                var searchedwords = SSC_List.OrderByDescending(c => c.NoOfContainingWords).Select(b => b.wordpuzzle).ToList();
                JArray puzzles = new JArray();
                foreach (var p in searchedwords)
                {
                    dynamic json = new JObject();
                    json.name = p.name;
                    json.id = p.id;
                    json.words = p.Words;
                    json.dimention = p.Dimention;
                    puzzles.Add(json);
                }
                ReturnValue.result = true;
                ReturnValue.puzzles = puzzles;
            }
            catch(Exception ex)
            {
                ReturnValue.result = false;
                ReturnValue.message = ex.Message;
            }
            return ReturnValue;
        }
    }
}
