﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrossWordPuzzleGenerator.Models;
using static CrossWordPuzzleGenerator.Models.ModelClasses;

namespace CrossWordPuzzleMaker.Controllers
{
    public class PuzzleGeneratorController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewData["Sample"] = "اساس،الفبای،پارسی،عربی،نخستین،برجسته،الفبای،فارسی،دارای،چهار،حرف،شاهنشاهی،واجهای،متناظر،لغاتی،عربی،استاندارد،وجود،فرهنگ،جنوب غربی،حروف،رسمی";
            ViewBag.Puzzle = "";
            ViewBag.IsGenerated = false;
            return View();
        }

        [HttpPost]
        // GET: PuzzleGenerator
        public ActionResult Index(FormCollection form)
        { 
            var Allwords = Request.Form["words"];
            Allwords = Allwords.Replace(",", "،");
            Allwords = Allwords.Replace("،،", "،");
            Allwords = Allwords.Replace(" ", "");
            Allwords = Allwords.Replace(".", "");
            int dimention = 0;
            ViewData["Sample"] = Allwords;
            char[,] ch;
            string[] words = Allwords.Split('،');
            Random rnd = new Random();
            words = words.OrderByDescending(c => c.Length).ToArray();
            dimention = words.First().Length;
            bool result = false;
            WordInView[,] wiv = new WordInView[dimention,dimention];
            while (true)
            {
                ch = new char[dimention, dimention];
                Utilities.EmptyPuzzle(ch);
                result = Utilities.GeneratePuzzle(words, ch);
                if (result)
                {
                    char[,] answers = new char[dimention, dimention];
                    for (int i = 0; i < ch.GetLength(0); i++)
                    {
                        for (int j = 0; j < ch.GetLength(1); j++)
                        {
                            if (ch[i, j] == '-' || ch[i, j] == '\0')
                            {
                                answers[i, j] = '\0';
                            }
                            else
                            {
                                answers[i, j] = ch[i, j];
                            }
                        }
                    }
                    ViewBag.Answer = answers;
                    Utilities.FillWithRandomLetters(ch);
                    break;
                }
                else
                {
                    Utilities.PrintPuzzle(ch);
                    //words = words.OrderBy(x => rnd.Next()).ToArray();
                    Utilities.EmptyPuzzle(ch);
                    dimention++;
                }
            }
            ViewBag.IsGenerated = true;
            ViewBag.Puzzle = ch;
            return View(wiv);//RedirectToAction("Puzzle");
        }
    }
}