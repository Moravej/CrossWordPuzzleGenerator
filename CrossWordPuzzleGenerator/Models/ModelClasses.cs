﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrossWordPuzzleGenerator.Models
{
    public class ModelClasses
    {
        public class Answer
        {
            public int dir { get; set; }
            public int NoOfSharedChars { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
            public string word { get; set; }
            public bool result { get; set; }
        }
        public class WordInPuzzle
        {
            public string Word { get; set; }
            public bool IsUsed { get; set; }
        }
        public class WordInView
        {
            public char character { get; set; }
            public bool IsRandom { get; set; }
        }
        public class SortSearchClass
        {
            public WordPuzzle wordpuzzle { get; set; }
            public int NoOfContainingWords { get; set; }
        }
    }
}