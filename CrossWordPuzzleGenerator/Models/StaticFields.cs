﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrossWordPuzzleGenerator.Models
{
    public static class StaticFields
    {
        public static class Directions
        {
            public static int Up
            {
                get
                {
                    return 0;
                }
            }
            public static int UpAndRight
            {
                get
                {
                    return 1;
                }
            }
            public static int Right
            {
                get
                {
                    return 2;
                }
            }
            public static int DownAndRight
            {
                get
                {
                    return 3;
                }
            }
            public static int Down
            {
                get
                {
                    return 4;
                }
            }
            public static int DownAndLeft
            {
                get
                {
                    return 5;
                }
            }
            public static int Left
            {
                get
                {
                    return 6;
                }
            }
            public static int UpAndLeft
            {
                get
                {
                    return 7;
                }
            }
            public static List<int> AllDirections
            {
                get
                {
                    return new List<int> { Right, Down, Left, Up, DownAndRight, UpAndRight, DownAndLeft, UpAndLeft };
                }
            }
            public static List<int> GeneralDirections
            {
                get
                {
                    return new List<int> { Right, Down, Left, Up };
                }
            }
        }
        public static class Other
        {
            public static int DeterminerFactor
            {
                get
                {
                    return 5;
                }
            }
        }
        public static List<char> PersianCharacters
        {
            get
            {
                return new List<char> {'ا','آ', 'ب', 'پ', 'ت' ,'ث', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س' ,'ش' ,'ص', 'ض' ,'ط' ,'ظ', 'ع', 'غ' ,'ف', 'ق', 'ک', 'گ', 'ل' ,'م', 'ن' ,'و', 'ه' ,'ی'};
            }
        }
        public static List<char> AllowedChars
        {
            get
            {
                return new List<char> { '.','،',' ','،' };
            }
        }
    }
}