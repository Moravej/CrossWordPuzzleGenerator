﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static CrossWordPuzzleGenerator.Models.ModelClasses;

namespace CrossWordPuzzleGenerator.Models
{
    public static class Utilities
    {
        public static Answer CanBePlaced(char[,] cross, int dir, int x, int y, string word)
        {
            Answer answer = new Answer();
            answer.word = word;
            answer.X = x;
            answer.Y = y;
            answer.dir = dir;
            int Shared = 0;
            if (dir == StaticFields.Directions.Down)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    try
                    {
                        if (cross[x + i, y] == '-')
                        {
                            answer.result = true;
                        }
                        else if (cross[x + i, y] == word[i])
                        {
                            Shared++;
                        }
                        else
                        {
                            answer.result = false;
                            break;
                        }
                    }
                    catch
                    {
                        answer.result = false;
                        Shared = 0;
                        break;
                    }
                }
            }
            //else if (dir == StaticFields.Directions.DownAndRight)
            //{
            //    for (int i = 0; i < word.Length; i++)
            //    {
            //        try
            //        {
            //            if (cross[x + i, y + i] == '-')
            //            {
            //                answer.result = true;
            //            }
            //            else if (cross[x + i, y + i] == word[i])
            //            {
            //                Shared++;
            //            }
            //            else
            //            {
            //                answer.result = false;
            //                break;
            //            }
            //        }
            //        catch
            //        {
            //            answer.result = false;
            //            Shared = 0;
            //            break;
            //        }
            //    }
            //}
            else if (dir == StaticFields.Directions.Right)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    try
                    {
                        if (cross[x, y + i] == '-')
                        {
                            answer.result = true;
                        }
                        else if (cross[x, y + i] == word[i])
                        {
                            Shared++;
                        }
                        else
                        {
                            answer.result = false;
                            break;
                        }
                    }
                    catch
                    {
                        answer.result = false;
                        Shared = 0;
                        break;
                    }
                }
            }
            //else if (dir == StaticFields.Directions.DownAndRight)
            //{
            //    for (int i = 0; i < word.Length; i++)
            //    {
            //        try
            //        {
            //            if (cross[x - i, y + i] == '-')
            //            {
            //                answer.result = true;
            //            }
            //            else if (cross[x - i, y + i] == word[i])
            //            {
            //                Shared++;
            //            }
            //            else
            //            {
            //                answer.result = false;
            //                break;
            //            }
            //        }
            //        catch
            //        {
            //            answer.result = false;
            //            Shared = 0;
            //            break;
            //        }
            //    }
            //}
            else if (dir == StaticFields.Directions.Up)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    try
                    {
                        if (cross[x - i, y] == '-')
                        {
                            answer.result = true;
                        }
                        else if (cross[x - i, y] == word[i])
                        {
                            Shared++;
                        }
                        else
                        {
                            answer.result = false;
                            break;
                        }
                    }
                    catch
                    {
                        answer.result = false;
                        Shared = 0;
                        break;
                    }
                }
            }
            //else if (dir == StaticFields.Directions.DownAndLeft)
            //{
            //    for (int i = 0; i < word.Length; i++)
            //    {
            //        try
            //        {
            //            if (cross[x - i, y - i] == '-')
            //            {
            //                answer.result = true;
            //            }
            //            else if (cross[x - i, y - i] == word[i])
            //            {
            //                Shared++;
            //            }
            //            else
            //            {
            //                answer.result = false;
            //                break;
            //            }
            //        }
            //        catch
            //        {
            //            answer.result = false;
            //            Shared = 0;
            //            break;
            //        }
            //    }
            //}
            else if (dir == StaticFields.Directions.Left)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    try
                    {
                        if (cross[x, y - i] == '-')
                        {
                            answer.result = true;
                        }
                        else if (cross[x, y - i] == word[i])
                        {
                            Shared++;
                        }
                        else
                        {
                            answer.result = false;
                            break;
                        }
                    }
                    catch
                    {
                        Shared = 0;
                        answer.result = false;
                        break;
                    }
                }
            }
            //else if (dir == StaticFields.Directions.UpAndLeft)
            //{
            //    for (int i = 0; i < word.Length; i++)
            //    {
            //        try
            //        {
            //            if (cross[x + i, y - i] == '-')
            //            {
            //                answer.result = true;
            //            }
            //            else if (cross[x + i, y - i] == word[i])
            //            {
            //                Shared++;
            //            }
            //            else
            //            {
            //                answer.result = false;
            //                break;
            //            }
            //        }
            //        catch
            //        {
            //            answer.result = false;
            //            Shared = 0;
            //            break;
            //        }
            //    }
            //}
            answer.NoOfSharedChars = Shared;
            return answer;
        }
        public static bool PlaceWord(Answer answer, char[,] cross)
        {
            bool checker = false;
            if (answer.dir == StaticFields.Directions.Up && answer.result)
            {
                for (int i = 0; i < answer.word.Length; i++)
                {
                    try
                    {
                        cross[answer.X - i, answer.Y] = answer.word[i];
                        checker = true;
                    }
                    catch
                    {
                        checker = false;
                        break;
                    }
                }
            }
            //else if (answer.dir == StaticFields.Directions.UpAndRight && answer.result)
            //{
            //    for (int i = 0; i < answer.word.Length; i++)
            //    {
            //        try
            //        {
            //            cross[answer.X + i, answer.Y + i] = answer.word[i];
            //            checker = true;
            //        }
            //        catch
            //        {
            //            checker = false;
            //            break;
            //        }
            //    }
            //}
            else if (answer.dir == StaticFields.Directions.Right && answer.result)
            {
                // dir == 3 means going right
                for (int i = 0; i < answer.word.Length; i++)
                {
                    try
                    {
                        cross[answer.X, answer.Y + i] = answer.word[i];
                        checker = true;
                    }
                    catch
                    {
                        checker = false;
                        break;
                    }
                }
            }
            //else if (answer.dir == StaticFields.Directions.DownAndRight && answer.result)
            //{
            //    for (int i = 0; i < answer.word.Length; i++)
            //    {
            //        try
            //        {
            //            cross[answer.X + i, answer.Y + i] = answer.word[i];
            //            checker = true;
            //        }
            //        catch
            //        {
            //            checker = false;
            //            break;
            //        }
            //    }
            //}
            else if (answer.dir == StaticFields.Directions.Down && answer.result)
            {
                //dir == 5 means going down 
                for (int i = 0; i < answer.word.Length; i++)
                {
                    try
                    {
                        cross[answer.X + i, answer.Y] = answer.word[i];
                        checker = true;
                    }
                    catch
                    {
                        checker = false;
                        break;
                    }
                }
            }
            //else if (answer.dir == StaticFields.Directions.DownAndLeft && answer.result)
            //{
            //    for (int i = 0; i < answer.word.Length; i++)
            //    {
            //        try
            //        {
            //            cross[answer.X + i, answer.Y - i] = answer.word[i];
            //            checker = true;
            //        }
            //        catch
            //        {
            //            checker = false;
            //            break;
            //        }
            //    }
            //}
            else if (answer.dir == StaticFields.Directions.Left && answer.result)
            {
                for (int i = 0; i < answer.word.Length; i++)
                {
                    try
                    {
                        cross[answer.X, answer.Y - i] = answer.word[i];
                        checker = true;
                    }
                    catch
                    {
                        checker = false;
                        break;
                    }
                }
            }
            //else if (answer.dir == StaticFields.Directions.UpAndLeft && answer.result)
            //{
            //    for (int i = 0; i < answer.word.Length; i++)
            //    {
            //        try
            //        {
            //            cross[answer.X - i, answer.Y - i] = answer.word[i];
            //            checker = true;
            //        }
            //        catch
            //        {
            //            checker = false;
            //            break;
            //        }
            //    }
            //}
            //PrintPuzzle(cross);
            return checker;
        }
        public static Answer FindBestPlace(string word, char[,] cross)
        {
            List<Answer> AvailableAnswers = new List<Answer>();
            for (int i = 0; i < cross.GetLength(0); i++)
            {
                for (int j = 0; j < cross.GetLength(1); j++)
                {
                    foreach (var a in StaticFields.Directions.GeneralDirections)
                    {
                        Answer ans = new Answer();
                        ans = CanBePlaced(cross, a, j, i, word);
                        if (ans.result == true)
                            AvailableAnswers.Add(ans);
                    }
                }
            }
            var t = AvailableAnswers.Where(a => a.NoOfSharedChars != a.word.Length).OrderByDescending(c => c.NoOfSharedChars).ToList().FirstOrDefault();
            if (t != null)
            {
                return t;
            }
            else
            {
                return null;
            }
        }
        public static bool GeneratePuzzle(string[] words, char[,] cross)
        {
            List<WordInPuzzle> wip = new List<WordInPuzzle>();
            foreach (var b in words)
            {
                WordInPuzzle w = new WordInPuzzle();
                w.Word = b;
                w.IsUsed = false;
                wip.Add(w);
            }
            PlaceFirstWord(cross, wip[0].Word);
            wip.RemoveAt(0);
            for (int k = 0; k < wip.Count; k++)
            {
                var BestAnswer = FindBestPlace(wip[k].Word, cross);
                if (BestAnswer != null)
                    wip[k].IsUsed = PlaceWord(BestAnswer, cross);
            }
            if (wip.Select(c => c.IsUsed).ToList().Contains(false))
                return false;
            else
                return true;
        }
        public static void EmptyPuzzle(char[,] ch)
        {
            for (int i = 0; i < ch.GetLength(0); i++)
            {
                for (int j = 0; j < ch.GetLength(1); j++)
                {
                    ch[i, j] = '-';
                }
            }
        }
        public static void FillWithRandomLetters(char[,] ch)
        {
                Random rand = new Random();
            for (int i = 0; i < ch.GetLength(0); i++)
            {
                for (int j = 0; j < ch.GetLength(1); j++)
                {
                    if (ch[i, j] == '-' || ch[i, j] == '\0')
                    {
                        ch[i, j] = StaticFields.PersianCharacters[rand.Next(0,32)];
                    }
                }
            }
        }
        public static void PlaceFirstWord(char[,] ch, string word)
        {
            Answer a = new Answer();
            a.word = word;
            a.X = 0;
            a.Y = 0;
            a.NoOfSharedChars = 0;
            a.result = true;
            a.dir = StaticFields.Directions.Right;
            PlaceWord(a, ch);
        }
        public static void PrintPuzzle(char[,] ch)
        {
            Console.WriteLine("");
            for (int i = 0; i < ch.GetLength(0); i++)
            {
                for (int j = 0; j < ch.GetLength(1); j++)
                {
                    if (ch[i, j] != '-')
                        Console.Write(ch[i, j] + " ");
                    else
                        Console.Write("  ");
                }
                Console.WriteLine("");
            }
        }
    }

}